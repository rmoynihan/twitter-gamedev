import requests
from flask import Flask, render_template, redirect, request
from get_glitches import glitches

app = Flask(__name__)

@app.route('/')
def start():
    title = request.args.get('game')

    # No game specified
    if title == None:
        return render_template('index.html')

    # Display dashboard for game
    else:
        game = {}
        game['title'] = title
        game['glitch_clusters'] = glitches(title)
        return render_template('dashboard.html', game=game)

app.run()
