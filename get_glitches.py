import pandas as pd
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import DBSCAN
import pprint

# Homemade modules
from cluster import GlitchCluster
from get_tweets import tweets

# Only used when reading from CSV
def find_glitches(df):
    glitches = []
    for index, row in df.iterrows():
        tweet = row['tweet']

        # Filter out tweets with less than four words of length
        if len(tweet.split()) > 3:
            # Remove any links attached onto the tweet and then append
            glitches.append(tweet.split('htt')[0])

    return glitches

# Find clusters in tweets
def find_clusters(glitches):
    # Perform TF-IDF transformation on raw text data
    vectorizer = TfidfVectorizer()
    gc = vectorizer.fit_transform(glitches)

    # Fit the DBSCAN algorithm to the data
    dbscan = DBSCAN(eps=.99, min_samples=2)
    labels = dbscan.fit_predict(gc)

    # Re-attach labels to tweets
    gc = dict(zip(glitches, labels.tolist()))

    return gc

# Given the labeled data, create cluster objects (see cluster.py for more)
def generate_clusters(glitches):
    clusters = []
    # Iterate over every found cluster
    for i in range(-1, max(list(glitches.values())) + 1):
        items = []
        # Find tweets that have the correct label
        for key, value in glitches.items():
            if value == i:
                items.append(key)

        # Create the new GlitchCluster object
        if len(items) > 2:
            clusters.append(GlitchCluster(items))

    # Sort the cluster objects by their priority score
    clusters.sort(key=lambda gc: gc.score, reverse=True)
    misc = clusters.pop(0)
    misc.score = -1
    clusters.append(misc)
    return clusters

# Main method
def glitches(title):
    ######################################
    # Choose the source for Twitter data #
    #                                    #

    # Read from a CSV file
    df = pd.read_csv("tweets.csv")
    glitches = find_glitches(df)

    #                                    #

    # Read from the Twitter API
    # glitches = tweets(title)

    #                                    #
    ######################################

    # Make method calls
    glitches = find_clusters(glitches)
    clusters = generate_clusters(glitches)

    return clusters

if __name__ == "__main__":
    glitches("Apex")
