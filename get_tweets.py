import tweepy
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from tweepy.error import TweepError
import time
import json
import pprint
import pandas as pd
import sys

# API configuration
def set_up_api():
    #consumer key, consumer secret, access token, access secret.
    ckey="bkOrrGYhzzQ2fiwNVYGlSkd19"
    csecret="jiRpPnUNaGnCsOrAR2A3MdpUuTApqrZySkTbhyH9zF5MfBdO8y"
    atoken="1101165771240153090-3xWky3HpH3DQ29FFAdhOeHdZxfx7z1"
    asecret="Kt3hYLKoUhF4DgG4Z7e5qpFT4M0MBIluTyc5IMkOQQkCv"

    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(atoken, asecret)

    api = tweepy.API(auth)
    return api

# Perform the search
def gather_tweets(api, query):
    tweets = tweepy.Cursor(api.search, q=query, lang='en').pages()
    return tweets

# Write tweets to CSV file
def create_csv(tweets):
    df = pd.DataFrame(columns=["tweet"])
    for tweet in tweets:
        df.loc[len(df)] = {'tweet':tweet}

    df.to_csv("tweets.csv")

# Main method
def tweets(title):
    api = set_up_api()
    queries = [title + " AND glitch", title + " AND bug"]

    tweets = []
    for query in queries:
        try:
            raw_tweets = gather_tweets(api, query=query)

            for page in raw_tweets:
                for tweet in page:
                    tweet = tweet._json
                    tweet = tweet['text']
                    if tweet not in tweets:
                        tweets.append(tweet)
        except TweepError as e:
            print(e)
            sys.exit()


    create_csv(tweets)
    return tweets

if __name__ == "__main__":
    tweets()
