import re
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import statistics as sta

# GlitchCluster object encapsulates information for each cluster found in the data
class GlitchCluster():
    def __init__(self, glitches):
        self.glitches = glitches

        self.freq = len(self.glitches)
        self.sent = self.calc_sent()

        self.score = round(self.freq + self.sent, 3)

    # Calculate the mean sentiment for the cluster, used as a tie-breaker for priority score
    def calc_sent(self):
        sent_list = []
        sid = SentimentIntensityAnalyzer()

        for g in self.glitches:
            sent_list.append(sid.polarity_scores(g)['neg'])

        return abs(sta.mean(sent_list))

    def __str__(self):
        return ("Priority score: " + str(self.score))
