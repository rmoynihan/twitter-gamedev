MIS 375 Project

Zayda Lopez
Riley Moynihan
June Pham

Application for game developers


Files:

__init__.py         Starts the web application and handles routing

get_tweets.py       Uses Twitter API to gather tweets related to the video game title and glitches/bugs

get_glitches.py     Cleans, vectorizes, and clusters the Twitter data

cluster.py          Class created to represent data about each cluster, also where sentiment analysis is performed

/templates/*.html   HTML/Jinja2 template pages for the web application

tweets.csv          Optional file, can use local tweet collection instead of using the Twitter API
